const env = require("dotenv").config({})
const express = require("express")
const { endsWith } = require("lodash")
const app = express()

app.use(express.json({ limit: "50mb" }))
app.get("/", (req, res) => {
   res.send("asdasdas")
})
app.get("/name", (req, res) => {
    res.send("jet")
})

app.delete("/name", (req, res) => {
    res.send("delete name")
})

app.disable("x-powered-by")
app.listen(process.env.EXPRESS_PORT, async () => {
   console.log(`App listening on port ${process.env.EXPRESS_PORT}!`)
})
